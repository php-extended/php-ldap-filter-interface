<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapFilterNodeSingleInterface interface file.
 *
 * This interface specifies a node of the criteria tree that represents a
 * single operand operator node (like a logical NOT node).
 *
 * @author Anastaszor
 */
interface LdapFilterNodeSingleInterface extends LdapFilterNodeInterface
{
	
	/**
	 * Gets the child node.
	 * 
	 * @return LdapFilterNodeInterface
	 */
	public function getChildNode() : LdapFilterNodeInterface;
	
}
