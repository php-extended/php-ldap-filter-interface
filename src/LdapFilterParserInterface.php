<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use PhpExtended\Parser\ParserInterface;

/**
 * LdapFilterParserInterface interface file.
 * 
 * This interface represents a parser for ldap filters.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<LdapFilterNodeMultiInterface>
 */
interface LdapFilterParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
