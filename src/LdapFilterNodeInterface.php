<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapFilterNodeInterface interface file.
 *
 * This interface specifies a node of the criteria tree for ldap criteria
 * filters.
 *
 * @author Anastaszor
 */
interface LdapFilterNodeInterface extends Stringable
{
	
	public const OP_AND = '&';
	public const OP_OR = '|';
	public const OP_NOT = '!';
	
	/**
	 * Gets whether this node is empty (and should not print anything for its
	 * canonical string representation).
	 *
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets a rfc compliant string representation of this filter.
	 *
	 * @return string
	 */
	public function getStringRepresentation() : string;
	
	/**
	 * Gets the operator of this filter.
	 *
	 * @return string
	 */
	public function getOperator() : string;
	
}
