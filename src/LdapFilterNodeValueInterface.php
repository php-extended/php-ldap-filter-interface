<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapFilterNodeValueInterface interface file.
 *
 * This interface specifies a node of the criteria tree that represents a
 * value (column=value).
 *
 * @author Anastaszor
 */
interface LdapFilterNodeValueInterface extends LdapFilterNodeInterface
{
	
	public const CMP_EQUALS = '=';
	public const CMP_APPROX = '~=';
	public const CMP_GREATER = '>=';
	public const CMP_LOWER = '<=';
	
	/**
	 * Gets the column name of this node.
	 * 
	 * @return string
	 */
	public function getColumn() : string;
	
	/**
	 * Gets the comparator of the node.
	 * 
	 * @return string
	 */
	public function getComparator() : string;
	
	/**
	 * Gets the effective value of this node.
	 * 
	 * @return string
	 */
	public function getValue() : string;
	
}
