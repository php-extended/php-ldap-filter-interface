<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Countable;
use Iterator;

/**
 * LdapFilterNodeMultiInterface interface file.
 *
 * This interface specifies a node of the criteria tree that represents a
 * multi operand operator node (like a logical AND or logical OR node).
 *
 * @author Anastaszor
 * @extends \Iterator<integer, LdapFilterNodeInterface>
 */
interface LdapFilterNodeMultiInterface extends Countable, Iterator, LdapFilterNodeInterface
{
	
	/**
	 * Adds a filter to this collection.
	 *
	 * @param LdapFilterNodeInterface $node
	 * @return static
	 */
	public function addNode(LdapFilterNodeInterface $node) : LdapFilterNodeMultiInterface;
	
	/**
	 * Appends all the nodes given by this array to this node.
	 * 
	 * @param array<integer, LdapFilterNodeInterface> $nodes
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addNodes(array $nodes) : LdapFilterNodeMultiInterface;
	
	/**
	 * Appends all the nodes given by this iterator to this node.
	 * 
	 * @param Iterator<LdapFilterNodeInterface> $nodes
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addNodeIterator(Iterator $nodes) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds a single value to this node for a straight comparison according
	 * to the operator of this node.
	 * 
	 * @param string $column
	 * @param null|boolean|integer|float|string $value
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addValue(string $column, $value, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds a single value to this node for a NOT comparison according to the
	 * operator of this node. This items is wrapped into a NOT node before
	 * being appended to this node.
	 * 
	 * @param string $column
	 * @param null|boolean|integer|float|string $value
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addNotValue(string $column, $value, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for an AND comparison. If this node is
	 * not an AND node, then all items are nested inside a new AND node.
	 * 
	 * @param string $column
	 * @param array<integer, null|boolean|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addAndValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for an AND comparison. If this node is
	 * not an AND node, then all items are nested inside a new AND node.
	 * 
	 * @param string $column
	 * @param Iterator<integer, null|boolean|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addAndIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for an NOT AND comparison. All the items
	 * are nested inside a NOT node that contains an AND node.
	 * 
	 * @param string $column
	 * @param array<integer, null|boolean|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addNandValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for an NOT AND comparison. All the items
	 * are nested inside a NOT node that contains an AND node.
	 * 
	 * @param string $column
	 * @param Iterator<integer, null|bool|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addNandIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for an OR comparison. If this node is
	 * not an OR node, then all items are nested inside a new OR node.
	 * 
	 * @param string $column
	 * @param array<integer, null|boolean|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return static
	 */
	public function addOrValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for an OR comparison. If this node is
	 * not an OR node, then all items are nested inside a new OR node.
	 * 
	 * @param string $column
	 * @param Iterator<integer, null|boolean|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return static
	 */
	public function addOrIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for an NOT OR comparison. All the items
	 * are nested inside a NOT node that contains an OR node.
	 * 
	 * @param string $column
	 * @param array<integer, null|boolean|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addNorValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for an NOT OR comparison. All the items
	 * are nested inside a NOT node that contains an OR node.
	 * 
	 * @param string $column
	 * @param Iterator<integer, null|boolean|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addNorIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for a XOR comparison. If this node is
	 * not a XOR node, then all items are nested inside a new XOR node.
	 * 
	 * @param string $column
	 * @param array<integer, null|boolean|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addXorValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Adds all the items to this node for a XOR comparison. If this node is
	 * not a XOR node, then all items are nested inside a new XOR node.
	 * 
	 * @param string $column
	 * @param Iterator<integer, null|boolean|integer|float|string> $values
	 * @param string $comparator one of LdapFilterNodeValueInterface constants
	 * @return LdapFilterNodeMultiInterface
	 */
	public function addXorIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface;
	
	/**
	 * Removes all the filter nodes that have the given column as comparison.
	 * This method removes also all nested filters for the given column and
	 * returns a completely new tree of filters.
	 * 
	 * @param string $column
	 * @return LdapFilterNodeMultiInterface
	 */
	public function removeNodesFor(string $column) : LdapFilterNodeMultiInterface;
	
}
